#!/bin/bash

#AUTHOR: RAVI DSOUZA
#SCRIPT: FIX BOOTSTRAP LINKS IN A SHARED WEBSERVER
#DATE: 02-MAR-2019


MYAPP=$1

if [ ! -d $MYAPP ]; then
        echo "looks like the project [$MYAPP] doesnt exist.. will now exit.."
        exit 2
fi


START_DIR=`pwd`
PROJECT_KEY=$(basename `pwd`)

STATIC_DIR=$MYAPP/static
TEMPLATE_DIR=$MYAPP/templates

if [ ! -f $PROJECT_KEY/settings.py ]; then
	echo "This script needs to run from within a project"
	exit 2

fi

function init_templates {
	if [ ! -d $TEMPLATE_DIR ]; then mkdir -p $TEMPLATE_DIR; fi && cd $TEMPLATE_DIR && if [ -L bootstrap ]; then unlink bootstrap ; fi && ln -s ../../${PROJECT_KEY}/templates/${PROJECT_KEY}/bootstrap
	cd ${START_DIR}
}

function init_static {
	if [ ! -d $STATIC_DIR ]; then mkdir -p $STATIC_DIR; fi && cd $STATIC_DIR && if [ -L bootstrap ]; then unlink bootstrap; fi && ln -s ../../${PROJECT_KEY}/static/bootstrap
        cd ${START_DIR}

}

if [ $# -ne 1 ]; then
	echo "invalid # of arguments.. usage: ./$0 <appname>"
	exit 2

fi

init_templates
init_static

