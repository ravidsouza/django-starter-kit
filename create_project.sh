#!/bin/bash

#AUTHOR: RAVI DSOUZA
#SCRIPT: START A NEW DJANGO PROJECT
#DATE: 02-MAR-2019


. conda.init

START_DIR=`dirname $0`

function create_links {
	cd $MYAPP && \
	ln -s ../create_new_app.sh && \
	ln -s ../migrate.sh && \
	ln -s ../runserver.sh
	ln -s ../fix_bootstrap_links.sh
	ln -s ../conda.init
	cd $START_DIR
}

function create_common_template {
	mkdir -p $MYAPP/templates/$MYAPP/bootstrap
}

if [ $# -ne 1 ]; then
        echo "invalid # of arguments.. usage: $0 <appname>"
        exit 2

fi

MYAPP=$1

if [ -d $MYAPP ]; then
        echo "looks like the project [$MYAPP] already exists.. will now exit.."
        exit 2
fi

django-admin startproject $MYAPP

create_links
create_common_template
