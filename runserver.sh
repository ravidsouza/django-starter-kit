#!/bin/bash

#AUTHOR: RAVI DSOUZA
#SCRIPT: START A DJANGO SERVER ON A CONDA ENVIRONMENT
#DATE: 02-MAR-2019

. conda.init

python manage.py runserver 0:8000


