#!/bin/bash

#AUTHOR: RAVI DSOUZA
#SCRIPT: CREATE A NEW DJANGO APP
#DATE: 02-MAR-2019



START_DIR=`pwd`
PROJECT_KEY=$(basename `pwd`)

if [ ! -f $PROJECT_KEY/settings.py ]; then
	echo "This script needs to run from within a project"
	exit 2

fi

function init_templates {
	mkdir -p ${MYAPP}/templates/${MYAPP}
	cd ${MYAPP}/templates && ln -s ../../${PROJECT_KEY}/templates/${PROJECT_KEY}/bootstrap
	cd ${START_DIR}
}

function init_static {
	mkdir -p ${MYAPP}/static/${MYAPP}
	cd ${MYAPP}/static && ln -s ../../${PROJECT_KEY}/static/bootstrap
        cd ${START_DIR}

}

function django_init {
	if [ ! -f $MYAPP/urls.py ]; then 
		cp ../django-init/urls.py $MYAPP/.
	fi


        if [ -f $MYAPP/views.py ]; then
		mv $MYAPP/views.py $MYAPP/views.py.backup
	fi
        cp ../django-init/views.py $MYAPP/views.py
	mycmd="sed -i \"s/__APPNAME__/$MYAPP/g\" $MYAPP/views.py"
	$(bash -c "${mycmd}")

        if [ ! -f $MYAPP/templates/$MYAPP/index.html ]; then
                cp ../django-init/index.html $MYAPP/templates/$MYAPP/index.html
		mycmd="sed -i \"s/__APPNAME__/$MYAPP/g\" $MYAPP/templates/$MYAPP/index.html"
		$(bash -c "${mycmd}")
        fi


}

function reminder {
	echo "--------------------"
	echo "DO NOT FORGET TO:"
	echo "1. ADD $MYAPP to INSTALL_APPS in $PROJECT_KEY/settings.py"
	echo "2. ADD $MYAPP TO $PROJECT_KEY/urls.py"
}

if [ $# -ne 1 ]; then
	echo "invalid # of arguments.. usage: ./$0 <appname>"
	exit 2

fi

MYAPP=$1

if [ -d $MYAPP ]; then
	echo "looks like the project [$MYAPP] already exists.. will now exit.."
	exit 2
fi

python manage.py startapp $1

init_templates
init_static
django_init


reminder
