# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.template import loader
from django.urls import reverse


def index(request):
	my_content={"__APPNAME__": "uk-active"}
	return render(request, '__APPNAME__/index.html', my_content)

