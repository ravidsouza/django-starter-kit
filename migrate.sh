#!/bin/bash

#AUTHOR: RAVI DSOUZA
#SCRIPT: RUN MIGRATE SCRIPT ON A DJANGO SERVER
#DATE: 02-MAR-2019


source /opt/anaconda2/etc/profile.d/conda.sh
conda activate base

python manage.py migrate
