#AUTHOR: RAVI DSOUZA
#TOOL: DJANGO APP AND APP STARTER KIT

#SIMPLE SHELL SCRIPT WHICH ALLOWS YOU TO CREATE DJANGO PROJECTS AND APPS EASILY

1. Create a project/portal using: ./create_project.sh
2. Create an app (must be run from inside a project) using: ./create_new_app.sh
3. Start the portal using ./runserver.sh from inside the project app

4. Ensure <PROJECT>/<PROJECT>/settings.py is configured.

5. your project template bootstrap directory can now hold your main header and css

